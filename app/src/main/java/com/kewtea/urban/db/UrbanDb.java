package com.kewtea.urban.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**

    table 1 : contents
                dealName, orgUrl, imgUrls, description, actionUrl, condition, category, city, source(supply), created, lastUpdate, bookmarked
    table 2 : eventHistory (log)
                time, type, msg, reference (userId)
    table 3 : (downloads)(media_playlist)
    table 4 :



 */
public class UrbanDb {

    public static final String TABLE1_CNTLIST ="";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CONTENTID = "contentId";  //contentID assigned by the server
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";  // car details in json, maker, model, year
    public static final String COLUMN_CARVALUECTX = "carvaluectx";  // json, estimated value,
    public static final String COLUMN_MAINIMGURL = "mainimgurl";  // json img, ext links
    public static final String COLUMN_SOURCEURLS = "sourceurls";  // json img, ext links
    public static final String COLUMN_REGISTERED = "registered";  // json date time created
    public static final String COLUMN_LASTUPDATE = "lastupdate";  // json last modified date time
    public static final String COLUMN_INBOX="inboxed"; //ctxAnalyzer가 판단해서 여기에 값을 표시하게 된다
    public static final String COLUMN_BOOKMARKED ="bookmarked"; //true(1), false(0)
    public static final String COLUMN_MYDRAFT ="drafted"; //true(1), false(0)
    public static final String COLUMN_MYPUBLISHED ="published"; //true(1), false(0)
    public static final String COLUMN_ACTIVE="active"; //중요한 아이템들만 표시해 둔다(동작을 빠르게 하기 위함)
    private static final String DATABASE_CREATE1 = "create table "
            + TABLE1_CNTLIST+" ("
            + COLUMN_ID+ " integer primary key autoincrement, "
            + COLUMN_CONTENTID + " text, "
            + COLUMN_TITLE+" text, "
            + COLUMN_DESCRIPTION+" text, "
            + COLUMN_CARVALUECTX+" text, "
            + COLUMN_MAINIMGURL+" text, "
            + COLUMN_SOURCEURLS+" text, "
            + COLUMN_REGISTERED+" integer not null, "
            + COLUMN_LASTUPDATE+" integer, "
            + COLUMN_INBOX+" integer, "
            + COLUMN_BOOKMARKED+" integer, "
            + COLUMN_MYDRAFT+" integer, "
            + COLUMN_MYPUBLISHED+" integer, "
            + COLUMN_ACTIVE+" integer"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        //database.execSQL(DATABASE_CREATE1);
        Log.i("Base:", "TABLE HAS BEEN CREATED");
    }
    public static void onUpgrade(SQLiteDatabase database, int oldVersion,int newVersion) {
        Log.w(UrbanDb.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        //database.execSQL("DROP TABLE IF EXISTS " + TABLE1_CNTLIST);
        onCreate(database);
    }

}
