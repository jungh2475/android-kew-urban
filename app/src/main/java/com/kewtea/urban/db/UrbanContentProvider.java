package com.kewtea.urban.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by jungh on 7/12/16.
 */
public class UrbanContentProvider extends ContentProvider {

    private UrbanDbHelper dbHelper;

    private static final String AUTHORITY ="net.freestyleworks.diagno.database";
    public static final String CONTENT_AUTHORITY="net.freestyleworks.diagno";
    public static final Uri CONTENT_URI_CONTENTLIST=Uri.parse("content://"+AUTHORITY+"/"+UrbanDb.TABLE1_CNTLIST);
    public static final String CONTENT_TYPE_CONTENTLIST = ContentResolver.CURSOR_DIR_BASE_TYPE+ "/"+UrbanDb.TABLE1_CNTLIST;
    public static final String CONTENT_ITEM_TYPE_CONTENTLIST = ContentResolver.CURSOR_ITEM_BASE_TYPE+ "/"+UrbanDb.TABLE1_CNTLIST;

    //UriMatcher cases
    private static final int CONTENT=1;
    private static final int CONTENT_ID=2;
    private static final int EVENT=3;
    private static final int EVENT_ID=4;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        //sURIMatcher.addURI(String auth, String path, int code);
        sURIMatcher.addURI(AUTHORITY, UrbanDb.TABLE1_CNTLIST, CONTENT);
        sURIMatcher.addURI(AUTHORITY, UrbanDb.TABLE1_CNTLIST+"/#", CONTENT_ID);
    }

    //public static final String[] table1_projection = {DiagnoDb.COLUMN_ID, DiagnoDb.COLUMN_CONTENTID, DiagnoDb.COLUMN_TITLE, DiagnoDb.COLUMN_DESCRIPTION,....}

    private void checkColumns(String tableName, String[] projection){}

    @Override
    public boolean onCreate() {
        dbHelper=new UrbanDbHelper(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        int uriType=sURIMatcher.match(uri);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long id = 0;
        int uriType = sURIMatcher.match(uri);
        id=db.insert(UrbanDb.TABLE1_CNTLIST,null,contentValues);
        return null;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] contentValues) {
        int numInserted;
        numInserted = contentValues.length;
        long id;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int uriType = sURIMatcher.match(uri);
        for(ContentValues cv:contentValues){
            id=db.insertOrThrow(UrbanDb.TABLE1_CNTLIST, null, cv);
            if(id<=0){throw new SQLException("Failed to insert row into " + uri);}
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return numInserted;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rowsUpdated = 0;
        String id;
        int uriType = sURIMatcher.match(uri);
        //delete bulk
        rowsUpdated=db.delete(UrbanDb.TABLE1_CNTLIST, selection, selectionArgs);
        //delete by id
        id=uri.getLastPathSegment();
        if(TextUtils.isEmpty(selection)){
            rowsUpdated=db.delete(UrbanDb.TABLE1_CNTLIST, UrbanDb.COLUMN_ID + "=" + id, null);
        } else {
            rowsUpdated=db.delete(UrbanDb.TABLE1_CNTLIST, UrbanDb.COLUMN_ID + "=" + id + " and " + selection, selectionArgs);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rowsUpdated = 0;
        String id;
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case CONTENT:
                rowsUpdated=db.update(UrbanDb.TABLE1_CNTLIST, contentValues,selection, selectionArgs);
                break;
            case CONTENT_ID:
                id=uri.getLastPathSegment();
                if(TextUtils.isEmpty(selection)){
                    rowsUpdated=db.update(UrbanDb.TABLE1_CNTLIST, contentValues, UrbanDb.COLUMN_ID+"="+id, null);
                } else {
                    rowsUpdated=db.update(UrbanDb.TABLE1_CNTLIST, contentValues, UrbanDb.COLUMN_ID+"="+id+" and "+selection, selectionArgs);
                }
                break;
            default:
                Log.i("Base:","update:uriType is not matched");
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }
}
