package com.kewtea.urban.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jungh on 7/12/16.
 */
public class UrbanDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Urban.db";
    private static final int DATABASE_VERSION = 1;

    public UrbanDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        UrbanDb.onCreate(sqLiteDatabase);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        UrbanDb.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }
}
