package com.kewtea.urban.util;

import org.json.JSONObject;

/**
 * Created by jungh on 7/12/16.
 */
public interface DownloadResult {
    void onResult(JSONObject object);
}
