package com.kewtea.urban.model;

/**
 * Created by jungh on 7/12/16.
 */
public class Content {

    public String title;
    public String shortDescription;


    public Content(){}
    public Content(String t, String d){
        this.title=t;
        this.shortDescription=d;
    }


}
