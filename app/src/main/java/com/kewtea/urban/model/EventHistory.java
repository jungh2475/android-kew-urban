package com.kewtea.urban.model;

/**
 * Created by jungh on 7/12/16.

 */
public class EventHistory {
    public long timestamp;
    public String type;
    public String msg;
    public long recorderId;
    public String reference;
    public int referenceId;

    public EventHistory(){}
    public EventHistory(String type, String msg){
        this.timestamp=System.currentTimeMillis();
        this.type=type;
        this.msg=msg;
    }
}
