package com.kewtea.urban.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by jungh on 7/12/16.
 */
public class SystemReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //TODO: MainService가 돌고 있지 않으면 새로 시작시킨다
        Log.i("kewUrban:","received sysIntent:"+intent.toString());
    }
}
