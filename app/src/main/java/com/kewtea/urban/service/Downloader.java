package com.kewtea.urban.service;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;

import com.kewtea.urban.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jungh on 7/22/16.
 *
 http://101apps.co.za/articles/using-the-downloadmanager-to-manage-your-downloads.html
 꼭 이것을 보고 구현 점검을 할것

 mService.reportStatus(int input)으로 상태를 보고한다

 */
public class Downloader {

    private Context mContext;
    private MainService mService;
    private DownloadManager downloadManager;
    private List<Download> downloadList;
    private BroadcastReceiver receiverDlComplete;
    private BroadcastReceiver receiverNotificationClicked;

    public static final int DIRECTION_UPLOAD=0;
    public static final int DIRECTION_DOWNLOAD=1;
    public static final int COMPLETED =2;
    public static final int PAUSED=3;
    public static final int INPROGRESS=4;
    public static final int NOTSTART=5;
    public static final int NOTREADY=6;

    public class Download{
        public long _id;  //id at contentProvider
        public long systemReference;  //android download id reference
        public int loadDirection;  //download or upload?
        public String fileType;
        public String fileName;
        public String outerUrl;
        public String innerUrl;
        public int status;
        public long startTime;
        public long stopTime;
        public Download(String url, int direction){
            if(downloadList!=null){
                this._id=downloadList.size();
                //차라리 컨텐츠 provider에 저장을 하고  _id값을 얻어 오는 것은 어떨런지?
            }  //이 부분의 코드에 문제가 있을 수 있음
            this.outerUrl=url;
            this.loadDirection=direction;
        }
    }

    public void init(Context context){
        this.mContext=context;
        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        downloadList = new ArrayList<>();

        IntentFilter filter_1 = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        IntentFilter filter_2 = new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED);
        receiverDlComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID,-1);
                if(reference==1L){
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(reference);
                    Cursor cursor = downloadManager.query(query);
                    //get the status of downloads
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                }
            }
        };
        receiverNotificationClicked = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String extraId=DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS;
                long[] references = intent.getLongArrayExtra(extraId);
                for(long reference: references){
                    //if(reference==downloadList.contains())
                }
            }
        };
        mContext.registerReceiver(receiverDlComplete,filter_1);
        mContext.registerReceiver(receiverNotificationClicked,filter_2);

    }

    public void attachService(MainService service){
        this.mService=service;
    }
    public void detachService(){
        mService=null;
    }

    private void getDownload(long id){
        //load from Content Provider
    }

    private long putDownload(Download load){
        //load.id;
        //downloadList...대신에 content provider에 저장해 두자
        return 0L;
    }

    public void requestTransfer(int i, int option){
        Download download = downloadList.get(i);
        Uri uri= Uri.parse(download.outerUrl);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        //if option : notification에서 보이게 할것인가 아닌가?
        request.setTitle(mContext.getResources().getString(R.string.app_name));
        //request.setDescription(); //download id
        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, download.fileName);  //겹치는 이름이면 어떻하지?
        //request.setDestinationUri(Uri.parse(downloadList.get(i).innerUrl));
        //if(option) request.setVisibleInDownloadsUi(true);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        download.systemReference = downloadManager.enqueue(request);
        //download status 값도 바꾸어 준다
    }

    public Downloader(Context context){
        init(context);
    }

    public void testing(){
        mService.reportStatus(MainService.SUBSERVICE_DOWNLOADER,0);
    }

}
