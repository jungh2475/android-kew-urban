package com.kewtea.urban.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.kewtea.urban.Config;
import com.kewtea.urban.MainActivity;
import com.kewtea.urban.R;
import com.kewtea.urban.ui.DetailActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jungh on 7/12/16.

    usage in Activity: declare mBound, mService, mServiceConnection =>when onStart :Context.bindService(intent,mServiceConnection), onStop

 toSend Broadcast Intent=new Intent(); : intent.addFlag, .setAction, =>sendBroadcast(intent)

 */
public class MainService extends Service {
    private final IBinder mBinder = new LocalBinder();
    List<Notification> notifications;
    Downloader downloader;

    public static final int SUBSERVICE_CTXMEASURE = 0;
    public static final int SUBSERVICE_DOWNLOADER = 1;
    public static final int SUBSERVICE_MEDIAPLAYER = 2;
    public static final int SUBSERVICE_SYNCADAPTER = 3;  //new content
    //그리고 SUBSERVICE_UI, SUBSERVICE_SYSTEM

    public class LocalBinder extends Binder {
        public MainService getService(){
            return MainService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private void init(){
        notifications= new ArrayList<>();
        //start CtxLogger if not, otherwise bind by messenger
        //start Broadcast Receiver & register
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_LOW);
        SystemReceiver systemReceiver = new SystemReceiver();
        registerReceiver(systemReceiver,filter);

        listeners=new ArrayList<>();

        //다운로더로 초기화 해야 하나?
        //downloader = new Downloader(this);
        downloader.attachService(this);

    }

    @Override
    public void onDestroy() {
        //downloader=null;
        super.onDestroy();
    }

    /*
        public service methods
        start/stop/get/set ctxLogger(loadSequence, configSensors)
        run syncadapter
     */

    //service Update Listener & Procgress_Ids
    List<ProgressListener> listeners;
    public static final int PROGRESS_PLAY=2;
    public static final int PROGRESS_COUNTER=3;

    public interface ProgressListener {
        public void update(int progressId, int value);
    }

    public int registerListener(ProgressListener listener){ //progre
        listeners.add(listener);
        return listeners.size();
    }

    public void removeListener(ProgressListener listener){
        if(listeners!=null && listeners.contains(listener)){ listeners.remove(listener);}
    }

    private void reportProgress(int progressId, int value){
        for(ProgressListener listener: listeners){
            listener.update(progressId,value);
        }
    }

    CountDownTimer countDownTimer;
    public static final long Count_M_FUTURE=5000L;  //long millisInFuture, long countDownInterval
    public static final long Count_M_INTERVAL = 1000L;

    public void startCountDownTimer(){
        countDownTimer=new CountDownTimer(Count_M_FUTURE,Count_M_INTERVAL ) {
            int value=0;
            @Override
            public void onTick(long l) {  //nil until finish 남은 시간을 보여줌
                value = (int) l;
                reportProgress(PROGRESS_COUNTER, value);
            }
            @Override
            public void onFinish() {
                reportProgress(PROGRESS_COUNTER,1);
            }
        };
        countDownTimer.start();
    }

    public void stopCountDownTimer(){
        if(countDownTimer!=null){countDownTimer.cancel();}
        countDownTimer=null;
    }

    private void resetCountDownTimer(){
        if(countDownTimer!=null){countDownTimer.cancel();}
        startCountDownTimer();
    }


    public void keyPressed(int id){
        resetCountDownTimer();
    }

    /*
        Notification issue & management
        https://developer.android.com/guide/topics/ui/notifiers/notifications.html
     */
    private int issueNotification(Notification notification){
        int i=-1;
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(Config.AppNotificationId,notification);
        notifications.add(notification);
        i=notifications.size();  //마지막에 추가가 되었으므로
        return i;
    }

    private Notification buildNotification(String title, String contentText, String targetAction, boolean vibration, boolean sound){
        int i=-1;
        NotificationCompat.Builder mBuilder =  //support v4 or v7?
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        //.setTicker(r.getString(R.string.notification_title))
                        .setSmallIcon(R.drawable.ic_menu_gallery)  //R.drawable.notification_icon
                        .setContentTitle(title)
                        .setContentText(contentText);

        Intent resultIntent = new Intent(this, DetailActivity.class);
        resultIntent.putExtra("requestId", targetAction);  //fragment_detail, itemId
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(DetailActivity.class); //<meta-dataeoeo신에  parentActivity에 값이 적혀 있을땐?
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);
        if(vibration){
            long[] v_pattern1 = {500,1000};
            mBuilder.setVibrate(v_pattern1);
        }
        if(sound){
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(uri);
        }
        if(targetAction.contains("postpone")){
            Intent myIntent = new Intent(this, MainService.class); //나한테 다시 돌아 오는 intent임 이 부분의 추가 구현이 필요함
            Bundle bundle = new Bundle();
            //add input arguments  : 예를 들어 나중에 reminder를 하기위해서 시간을 조정한다 등 등.....
            //PendingIntent childPIntent = PendingIntent.getActivity(MainService.this, 0, myIntent, 0); //after Android 4.1  getActivity
            //(Context context,int requestCode,Intent intent,int flags)
            PendingIntent childPIntent = PendingIntent.getService(this,0,myIntent,0); //https://developer.android.com/reference/android/app/PendingIntent.html#FLAG_ONE_SHOT
            mBuilder.addAction(R.drawable.ic_menu_send, "Remind Later", childPIntent);
        }
        return mBuilder.build();
    }



    private void cancelNotification(int notiItem){

        if(notifications==null || notifications.size()<= notiItem){
            Log.w("kewUrban:","wrong request on notificatgion removal");
            return;
        }

        //if int 0, cancel all
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(notiItem<=0){ //전체 삭제
            mNotificationManager.cancel(Config.AppNotificationId);
            return;
        }
        //TODO: 특정 순서의 것을 없애면, 그 뒤의 것들은 다 삭제하고, 그 하나전의 값으로 환원 한다
        if(notifications.size()>=notiItem){
            for(int k=notiItem;k<notifications.size();k++){
                notifications.remove(k);
            }
            if(notiItem-1>=0){
                Notification notification=notifications.get(notiItem-1);
                issueNotification(notification);
            }
             //이때 Vibration/Ring을 제거해야하는 것 아닌가?
        }

    }


    /*
        download manager

     */

    public void reportStatus(int reporter, int input){
        switch(input){
            case 0:
                break;
            default:
                break;
        }
    }


}
