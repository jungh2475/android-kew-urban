package com.kewtea.urban;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.kewtea.urban.ui.CoverActivity;
import com.kewtea.urban.ui.Detail2Activity;
import com.kewtea.urban.ui.DetailActivity;
import com.kewtea.urban.ui.IntroActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //coverActivity에서 이 것이 나중에 호출이 되어져야 합니다.
        PreferenceManager.setDefaultValues(this, R.xml.preference, false);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks.
        int id = item.getItemId();
        pageOpen(id);
        /*
        switch(id){
            case R.id.nav_intro:
                break;
            case R.id.nav_login:
                break;
            case R.id.nav_cover:
                pageOpen(id);
                break;
            case R.id.nav_detail:
                break;
            case R.id.nav_preference:
                break;
            case R.id.nav_slideshow:
                break;
            case R.id.nav_survey:
                break;
            case R.id.nav_mediaplayer:
                break;
            case R.id.nav_gmap:
                break;
            case R.id.nav_devsummary:
                break;
            case R.id.nav_advsetting:
                break;
            case R.id.nav_tagging:
                break;
            default:
                Log.i("KewUrban","no matching case");
                break;
        }
        */
        /*if (id == R.id.nav_camera) {
        } else if (id == R.id.nav_gallery) {
        } else if (id == R.id.nav_slideshow) {
        } else if (id == R.id.nav_manage) {
        } else if (id == R.id.nav_share) {
        } else if (id == R.id.nav_send) {
        }
        */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void pageOpen(int req){
        Intent intent = null;
        switch(req){
            case R.id.nav_cover:
                intent = new Intent(getApplicationContext(), CoverActivity.class);
                break;
            case R.id.nav_detail:
                intent = new Intent(getApplicationContext(), DetailActivity.class);
                break;
            case R.id.nav_preference:
                intent = new Intent(getApplicationContext(), Detail2Activity.class);
                intent.putExtra("fragmentId",R.id.nav_preference);
                break;
            case R.id.nav_intro:
                intent= new Intent(getApplicationContext(),IntroActivity.class);
                break;
            case R.id.nav_login:
                intent = new Intent(getApplicationContext(), Detail2Activity.class);
                intent.putExtra("fragmentId",R.id.nav_login);
            default:
                Log.i("KewUrban:","no_match");
                break;
        }
        //intent.putExtra(requestCode, reqCode);
        if(intent!=null && intent.resolveActivity(getPackageManager())!=null) {
            startActivity(intent);
        }
    }

}
