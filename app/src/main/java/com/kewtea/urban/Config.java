package com.kewtea.urban;

/**
        여기 외에 찾아 봐야 할곳은 Resource/Value (lanuage localization 관련)

 */
public class Config {


    private Config() {
    }

    public static final String AppName="kewUrban";
    public static final String YOUTUBE_API_KEY = "YOUR API KEY";
    public static final String kewUrl ="http://api.kewtea.com/urban/api1";
    public static final String swVer="1";
    public static final String dbSheetUrl="https://docs.google.com/spreadsheets/d/1Ph7mBOth99pOYWkrZq8r34MFlVKTp-GYS1y61HgAWwg"; //then go to content

    public static final String EXTRA_ADD_ON = "extra add ons";

    public static final int AppNotificationId = 2745;
    public static final String PREF_COVERLIST="cover page items";
    public static final String PREF_NAVIGATIONMENU_EXTRALIST="add on items";
    public static final String PREF_LAST_LOGIN_TIME ="last log in time";
    public static final String PREF_LANGUAGE ="language";
    public static final String PREF_USERNAME ="user name";
    public static final String PREF_SW_VERSION ="sw version";

}


/*
    youtube api key
    https://www.sitepoint.com/using-the-youtube-api-to-embed-video-in-an-android-app/
    https://console.developers.google.com/
    Youtube Data API v3, Credentials(OAuth2), API key=>Android Key with your package(com.kewtea.urban)
    keytool -list -v -keystore ~/.android/debug.keystore => SHA-1 certificate fingerprint (Enter android as the password)
    debug keystore is located at ~/.android/debug.keystore  => opy the SHA-1 fingerprint and paste it on the Google Developer Console and click on the Create button
    =>got api key finally

    compile 'com.google.apis:google-api-services-youtube:v3-rev176-1.22.0' or compile files('libs/YouTubeAndroidPlayerApi.jar')

 */