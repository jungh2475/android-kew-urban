package com.kewtea.urban.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kewtea.urban.R;

import org.w3c.dom.Text;

/**
 * Created by jungh on 7/12/16.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    private int signType;  //signUp 0, signin 1

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login_signup, container, false);
        buildSignForm(view);
        return view;
    }

    private void buildSignForm(View view){
        LinearLayout signForm = (LinearLayout) view.findViewById(R.id.layout_signform);

        //fragment.setArguments(bundle);이렇게 보낸것을 받아서 깐다
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            signType = bundle.getInt("signType", -1);
        }

        TextView titleText = (TextView) view.findViewById(R.id.sign_forgot_password);
        TextView leftText = (TextView) view.findViewById(R.id.sign_forgot_password);
        TextView rightText = (TextView) view.findViewById(R.id.sign_signin);

        signForm.removeAllViewsInLayout();
        switch(signType){
            case 0:
                leftText.setText(getResources().getString(R.string.sign_forgot_password));
                rightText.setText(getResources().getString(R.string.sign_create_account));
                break;
            case 1:
                leftText.setText("");
                rightText.setText(getResources().getString(R.string.sign_already_member));
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {

    }
}
