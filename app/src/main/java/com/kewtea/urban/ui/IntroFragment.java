package com.kewtea.urban.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.kewtea.urban.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by junghlee on 7/19/16.

    mode 1: default : img + text
    mode 2: videoView
    mode 3: select from spinner (ArrayAdapter<CharSeq>)
    mode 4: checkbox selector (from list) 여기서  dialogFragment를 호출하게 한다. activity에서 할 필요 없다 dialogFragment.show(getFragmentManager(), "dialog");
    mode 5: login

    bundle 값을 언제/어느 callbak에서 읽어 와야 하는가? : Retrieve the arguments stored in the Bundle as part of the onCreate or if you really need to the onCreateView event handlers.
    그러면 activity에서는 bundle을 어떻게 넣어 주는가 setArgument

    call Fragment public methods from activity : FragmentById() or findFragmentByTag()
    define interface within fragment & implement within activity (onAttach(activity) try{ }mCallback = (OnHeadlineSelectedListener) activity;



 */
public class IntroFragment extends Fragment implements View.OnClickListener, MyDialogFragment.NoticeDialogListener, AdapterView.OnItemSelectedListener {

    public static final int INTRO_MODE1_DEFAULT=0;
    public static final int INTRO_MODE2_VIDEO=1;
    public static final int INTRO_MODE3_SPINNER=2;
    public static final int INTRO_MODE4_CHECKLIST=3;
    public static final int INTRO_MODE5_LOGIN=4;

    Context mContext;
    int layoutId;
    //list<ViewProperty> + behaviour(click)
    MyDialogFragment.NoticeDialogListener mListener;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        mContext=getActivity();
        Bundle bundle=this.getArguments();
        if(bundle!=null){ Log.i("kewUrban:","bundle exists at onCreate");}
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (MyDialogFragment.NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle=this.getArguments();
        if(bundle!=null){ Log.i("kewUrban:","bundle exists at onCreateView");}

        int resourceId=0;
        try { resourceId= R.layout.fragment_intro; } catch (Exception e) { e.printStackTrace(); }

        View view = inflater.inflate(resourceId,container,false);
        buildView(0,view);
        return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void buildView(int type, View view){

        switch(type) {
            case INTRO_MODE1_DEFAULT: //type1
                break;
            case INTRO_MODE2_VIDEO: //type2
                break;
            case INTRO_MODE3_SPINNER: //type3
                Spinner spinner = (Spinner) view.findViewById(0);  //<Spinner android:id="@+id/spinner" android:prompt="@string/spinner_title"/>
                //spinner.-> init categories
                List<String> categories= new ArrayList<>();
                categories.add("abc");
                categories.add("Business Services");
                spinner.setOnItemSelectedListener(this); //alternatively:   spinner.setOnItemClickListener(this);
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
                //ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),R.array.planets_array, android.R.layout.simple_spinner_item);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); //// Drop down layout style - list view with radio button
                // attaching data adapter to spinner
                spinner.setAdapter(dataAdapter);
                break;
            case INTRO_MODE4_CHECKLIST: //type4
                Button btn=(Button) view.findViewById(0);  //R.id.intro_btn
                btn.setOnClickListener(this);
                break;
            case INTRO_MODE5_LOGIN:
                //set SignIn or set SignUp
                int signType=0;
                if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("has_account",false)){signType=1;};
                buildSignForm(view,signType);
                break;
            default:
                break;
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.action0:
                break;
            case 1:      //R.id.intro_btn
                //mContext
                //if exisiting dialog fragment -remove
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                MyDialogFragment dialogFragment = new MyDialogFragment();
                dialogFragment.show(getFragmentManager(), "hihi");
                //dialogFragment.multiChoiceClickListener();
                break;
            default:
                break;
        }
    }



    public void showNoticeDialog(){

    }

    @Override
    public void onDialogClick(DialogFragment dialog, int option) {
        showNoticeDialog();
    }

    //dialog.show(fragmentManager,inputs)

    private void buildSignForm(View view, int signType){  //0 signUp, 1 signIn
        LinearLayout signForm = (LinearLayout) view.findViewById(R.id.layout_signform);

        //fragment.setArguments(bundle);이렇게 보낸것을 받아서 깐다
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            signType = bundle.getInt("signType", -1);
        }

        TextView titleText = (TextView) view.findViewById(R.id.sign_forgot_password);
        TextView leftText = (TextView) view.findViewById(R.id.sign_forgot_password);
        TextView rightText = (TextView) view.findViewById(R.id.sign_signin);

        signForm.removeAllViewsInLayout();
        switch(signType){
            case 0:
                leftText.setText(getResources().getString(R.string.sign_forgot_password));
                rightText.setText(getResources().getString(R.string.sign_create_account));
                break;
            case 1:
                leftText.setText("");
                rightText.setText(getResources().getString(R.string.sign_already_member));
                break;
            default:
                break;
        }
    }

    //for spinner click
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

}
