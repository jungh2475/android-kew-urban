package com.kewtea.urban.ui;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import com.kewtea.urban.R;

/**
 * Created by junghlee on 7/16/16.
 *
 제가 파악한바로는 surfaceview로 videoView가 만들어졌고
 custom videoview를 만들려면 surfaceview+mediaplayer를 사용하는걸로
 mediaplayer 객체를 직접 사용할 경우 볼륨조절이라던가, public 함수를 모두 쓸 수 있죠.
 videoview 에는 볼륨 조절 같은 함수가 개방되어 있지 않습니다
 Surface의 MediaController는 SufaceHolder안에 생기는 반면  VedioView의 MediaController는 Window의 화면 구성 아래에 생긴다는 것 정도

 mediaPlayer => use SurfaceView, implements SurfaceHolder.Callback, OnPreparedListener(MediaPlayer mp)

 http://code.tutsplus.com/tutorials/streaming-video-in-android-apps--cms-19888
 MediaPlayer, SurfaceView, SurfaceHolder= vidSurface.getHolder(); vidHolder.addCallback(this);
 try { }mediaPlayer = new MediaPlayer(); .setDisplay(vidHolder); .prepare();.setOnPreparedListener(this);
 mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC); mediaPlayer.start();

 */
public class FragmentVideo extends Fragment {

    private VideoView videoView;
    private MediaController mediaController;
    private int position;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(0,container,false);
        videoView=(VideoView) view.findViewById(0); //R.id.videoView);
        mediaController = new MediaController(getActivity());
        try{
            videoView.setMediaController(mediaController);

            videoView.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.kewgardens));
            //videoView.setVideoPath("http://www.android-examples.com/wp-content/uploads/2016/01/sample_video.3gp");
            //for youtube url : <com.google.android.youtube.player.YouTubePlayerView
            //videoView.start();
            videoView.requestFocus();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {
                    videoView.start();
                }
            });
            //videoView.setOnCompletionListener();
            //setOnError


                }catch(Exception e){}
        return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        videoView.seekTo(position);


        try{
        videoView.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.kewgardens));
        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.start();
            }
        });
        //videoView.setOnCompletionListener();
        //setOnError


        }catch(Exception e){}
    }

    @Override
    public void onPause() {

        videoView.pause();
        position = videoView.getCurrentPosition(); //save position
        super.onPause();
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
