package com.kewtea.urban.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.kewtea.urban.R;

/**
 * Created by jungh on 7/2/16.

    AB0(actionBar) : no Action Bar,  optional_FAB_bottom
    AB1 : default, optional_FAB_tolbar
    AB2 : top-imageView, transparent toolbar, optional_FAB_toolbar
    AB3 : with Tabs  : viewPager, optional_FAB_bottom
    AB4 : middle-toolbar + top-imageView, optional_FAB_bottom

    layout id  (R.layout.x)
    theme & style id  (R.style.y)
    fragment name -> fragment_layout_id (R.layout.fragment_z)


 */
public class DetailActivity extends AppCompatActivity {





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


}
