package com.kewtea.urban.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.kewtea.urban.R;

import java.util.ArrayList;

/**
 * Created by jungh on 7/19/16.
 *
 *
 https://github.com/codepath/android_guides/wiki/Using-DialogFragment
 */


public class MyDialogFragment extends DialogFragment {

    ArrayList selectedItems = null;
    CharSequence[] candidates = { "BMW", "AUDI", "MAHINDRA", "TATA", "MARUTI" };
    ArrayList selects = new ArrayList();

    DialogInterface.OnMultiChoiceClickListener multiChoiceClickListener= new DialogInterface.OnMultiChoiceClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which, boolean isChecked) {  //몇번째 것이 체크되어져 있는가
            if (isChecked){ //selectedItems.add(which);
            }else {  //if (selectedItems.contains(which)) {selectedItems.remove(Integer.valueOf(which));
            }
        }
    };

    public void multiChoiceClickListener() {
    }

    public interface NoticeDialogListener {
        public void onDialogClick(DialogFragment dialog, int option);
    }

    NoticeDialogListener mListener;

    public static MyDialogFragment newInstance(int title, ArrayList<String> candidates, String action){
        MyDialogFragment fragment = new MyDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putStringArrayList("candidates", candidates);
        args.putString("action", action);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int style = DialogFragment.STYLE_NORMAL;
        int theme = android.R.style.Theme_Holo_Light;
        setStyle(style, theme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);

        selectedItems= new ArrayList();
        int title = getArguments().getInt("title");

        //TODO: save하면 sharedPreference에 저장한다. cancel하면 그냥  나오면 된다 dimiss
        //
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                //.setIcon(R.drawable.ic_menu_gallery)  //R.drawable.alert_dialog_icon
                //.setMessage()
                //.setAdapter(adapter, listener) vs below vs .setItems
                .setMultiChoiceItems(candidates,null, multiChoiceClickListener) //implement onClick  R.array.candidates;
                .setPositiveButton(R.string.alert_dialog_ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                //((FragmentAlertDialog)getActivity()).doPositiveClick();
                                Log.i("kewUrban:","btn clicked:"+whichButton);
                                //TODO: save the list to sharedPreference
                                //PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("","").commit();
                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton(R.string.alert_dialog_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Log.i("kewUrban:","btn clicked:"+whichButton);
                                dialog.dismiss();
                            }
                        }
                );

        return builder.create();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);



    }

    public void updateDialog(){
        Dialog dialog=getDialog();  //이것과 dialogFragment의 차이점은?
    }
}