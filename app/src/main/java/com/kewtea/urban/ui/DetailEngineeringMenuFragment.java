package com.kewtea.urban.ui;

import android.app.Fragment;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kewtea.urban.R;
import com.kewtea.urban.model.Content;
import com.kewtea.urban.util.DownloadJsonTask;
import com.kewtea.urban.util.DownloadResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Created by jungh on 7/12/16.
 */
public class DetailEngineeringMenuFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_engineeringmenu, container,false);
        return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.action0:
                break;
            default:
                break;
        }

    }

    //이것을 다른 thread안에서 돌려서 값을 받아야 하나?
    /*
            1.loadCsv
            2.writeData
     */

    private void init(){
        //각종 경로명들을 체크해 복 이상한 것이 있는 지 살펴 보자
        String file = Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileDir = getActivity().getFilesDir().getAbsolutePath();
        String externalMediafileDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath();
        Log.i("kewUrban:","[file]"+file+"[fileDir]"+fileDir+"[extMediaDirPath]"+externalMediafileDir);

        //onClickListener를 달고


    }

    private List<String[]> loadCsv(String fileName){
        String filePath= Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePathName=filePath+"/"+fileName;
        List<String[]> list = new ArrayList<>();
        try{
            File file = new File(filePathName);
            if(file.exists()){
                CSVReader csvReader = new CSVReader(new FileReader(filePathName));
                list=csvReader.readAll();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    //http://www.telerik.com/blogs/google-spreadsheet-as-data-source-android

    private List<Content> loadGSheet(String sheetUrl) throws IOException {
        String gSheetId="1Ph7mBOth99pOYWkrZq8r34MFlVKTp-GYS1y61HgAWwg";
        List<Content> listdata = new ArrayList<>();
        //extract_id => converttoJsonUrl https://spreadsheets.google.com/tq?key=1tJ64Y8hje0ui4ap9U33h3KWwpxT_-JuVMSZzxD2Er8k
        //https://spreadsheets.google.com/tq?key=1Ph7mBOth99pOYWkrZq8r34MFlVKTp-GYS1y61HgAWwg
        new DownloadJsonTask(new DownloadResult() {
            @Override
            public void onResult(JSONObject object) {
                processJson(object);
            }
        }).execute("https://spreadsheets.google.com/tq?key="+gSheetId);  //url

        return listdata;
    }

    private List<Content> processJson(JSONObject object) {
        List<Content> contents = new ArrayList<>();
        try {
            JSONArray rows = object.getJSONArray("rows");

            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");

                int position = columns.getJSONObject(0).getInt("v");
                String name = columns.getJSONObject(1).getString("v");
                int wins = columns.getJSONObject(3).getInt("v");
                int draws = columns.getJSONObject(4).getInt("v");
                int losses = columns.getJSONObject(5).getInt("v");
                int points = columns.getJSONObject(19).getInt("v");
                Content content = new Content();
                contents.add(content);
                //Team team = new Team(position, name, wins, draws, losses, points);

                //teams.add(team);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contents;
    }

    /*
    private void writeData(String tableName, List<Content> entryList){

        List<String[]> entryList => List<Content> contents
        Content content (init)-(forLoop)
        content.property0=entryList.get(i)[0];
        content.property1=entryList.get(i)[1];
        contents.add(content);
    }
    */



    private void writeData(String tableName, List<String[]> entryList){
        //첫열이 반드시 데이터 header명으로 채워져 있어야함
        String[] projection ={};
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        ContentValues contentValues= new ContentValues();
        switch (tableName){
            default:
                //if(projection.length!=){return;}  //Csv column number mismatch-checking with db_table_column size
                //if(list.get(0)[k].equals(projection[k]))//그리고 맨 첫열의 헤더 이름들과 테이블 필드명이 같은지 체크해본다
                break;
        }

        //TODO: write to ContentProvider
        /*
        for(int i=1;i<entryList.size();i++){
            contentValues.put(ColumnKeyField0, entryList.get(i)[0]);
            contentValues.put(ColumnKeyField1, entryList.get(i)[1]);
            contentValues.put(ColumnKeyField2, entryList.get(i)[2]);
            operations.add(ContentProviderOperation.newInsert()
                    .withValue(contentValues).build());
        }
        */

        //왕창넣는 것이 아닌 하나만 넣는 경우 : uri=getActivity().getContentResolver().insert(DiagnoContentProvider.CONTENT_URI_CONTENTLIST, contentValues);

        try {
            getActivity().getContentResolver().applyBatch("com.kewtea.urban.db",operations);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }
}
