package com.kewtea.urban.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.kewtea.urban.Config;
import com.kewtea.urban.R;
import com.kewtea.urban.service.MainService;

import java.util.List;

/**
 * Created by junghlee on 8/3/16.

    UI에서만 동작하면 되는 데 굳이 Service에서 Trigger를 받아 올 필요는 없고, 다만 MainService에 로그만 전달 하면 된다.
    여기에 이제 viewPager를 추가하여,
    시나리오 1 :  슬라이드쇼임. touch를 안하고 오래 있으면 자동으로 슬라이드쇼가 시작됨. 터치하면 화면이 멈춤
    시나리오 2 :  메뉴가 보임. touch를 안하고 오래 있으면 자동으로 슬라이드쇼가 시작됨. 터치하면, 다시 메뉴가 보임.

 이것에 맞추어서 시나리오들을 다시 다 짜야함
 */
public class SlideActivity extends AppCompatActivity {

    public static final String SCENARIO ="scenario";
    public static final int SCENARIO_SLIDESTOP = 1;
    public static final int SCENARIO_SLIDEORMENU = 2;

    int scenario=SCENARIO_SLIDESTOP; //default
    boolean playMode = false;  //slideShow(true) or menu(false)
    boolean timerRunning = false;
    Handler handler;
    final long keyWait=2000L;
    final long slideInterval=2000L;
    int position;
    int listSize;
    int period;
    List<String> playList;

    /* 나중에 서비스로 로그 정보 보낼때나 쓰면 된다
    boolean mBound=false;   MainService mService;
    Intent intent = new Intent(this, MainService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        private ServiceConnection mConnection = new ServiceConnection() {

    */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        /*

        start From Meno Mode 그리고 key입력이 수초 없으면 자동으로 슬라이드쇼 시작함
        다시 터치가 있으면 menu모드로 빠져 나왔다가 수초간 터치가 없으면 슬라이드쇼 진행.
        터치가 있을때마다 timer가 delay된다

         */

        if(getIntent().getExtras()!=null){
            Bundle bundle =  getIntent().getExtras();
            scenario=bundle.getInt(SCENARIO,SCENARIO_SLIDESTOP);
        }
        handler = new Handler();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //full_screen no action bar
        playMode=false; //menuMode
        showMenu();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startSlideshow();
            }
        },keyWait);
        //add touch listener
        View view = findViewById(R.id.viewPager);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keyTouched(v);
            }
        });

    }

    @Override
    protected void onResume() {
        if(playMode){

        }else{

        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacksAndMessages(null);
        super.onPause();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void keyTouched(View view){
        //menuMode면 이미 앞에 있는 타이머를 죽이고, 새 타이머가 동작하게 한다. slideShow중이면 menuMode로 간다
        handler.removeCallbacksAndMessages(null);  //이전의 모든 것을 제거함

        if(view.getId()==R.id.action0){ //exit 버튼이면 완전히 나가서 mainParent로 복귀한다

            return;
        }

        if(!playMode){  //menuMode인 경우
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startSlideshow();
                }
            },keyWait);
        }else{  //슬라이드쇼인 경우
            showMenu();
        }

    }

    private void showMenu(){
        playMode=false;
        //코드 여기요 ....

    }
    private void startSlideshow(){
        playMode=true;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                playNext();
            }
        },slideInterval);
        playNext();

    }

    private void playNext(){
        //여기서 viewPager 해당 페이지로 이동시킨다
    }

}
