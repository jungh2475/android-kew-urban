package com.kewtea.urban.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.kewtea.urban.R;

/**
 * Created by jungh on 7/12/16.

 https://github.com/PaoloRotolo/AppIntro or
 http://www.androidhive.info/2016/05/android-build-intro-slider-app/
 https://medium.com/android-news/creating-an-intro-screen-for-your-app-using-viewpager-pagetransformer-9950517ea04f#.9aaye08g8

 slide 3 + signUp(signIn, loginwith Google/Facebook) or Signin(forgotpassword, signUp)

 */
public class IntroActivity extends AppIntro {

    LoginFragment loginFragment;
    IntroFragment citySelectFragment;

    @Override
    public void init(@Nullable Bundle savedInstanceState) {

        Bundle bundle_citySelect = new Bundle();
        bundle_citySelect.putString("","");
        citySelectFragment.setArguments(bundle_citySelect);

        String title=getResources().getString(R.string.intro_title1);
        String description = getResources().getString(R.string.intro_description1);
        loginFragment= new LoginFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("signType", 1);
        loginFragment.setArguments(bundle);

        //
        addSlide(AppIntroFragment.newInstance(title,description,R.drawable.sky, Color.parseColor("#AF91B5")));
        addSlide(AppIntroFragment.newInstance(title,description,R.drawable.sky, Color.parseColor("#DF91B5")));
        addSlide(AppIntroFragment.newInstance(title,description,R.drawable.sky, Color.parseColor("#F191B5")));
        //addSlide(citySelectFragment);
        addSlide(loginFragment);  //works with support v4 fragment



        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));
        setSlideOverAnimation();
        showSkipButton(false);
        showDoneButton(false);
        Log.i("kewUrban:","intro init completed");
        //setVibration, alpha
    }


    @Override
    public void onSkipPressed() {
    }

    @Override
    public void onDonePressed() {
    }

    private void showDialog(){
        new MyDialogFragment().show(getSupportFragmentManager(),"tag");
    }

}
