package com.kewtea.urban.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.kewtea.urban.Config;
import com.kewtea.urban.R;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jungh on 8/8/16.

    extends from MainActtivity.java : navigationDraw, CollapsingToolBar, viewPager + tabLayout (http://www.truiton.com/2015/06/android-tabs-example-fragments-viewpager/, http://susemi99.kr/3399)
    and multiple RecycleViews

    inputs from SharedPreference
     tabName,image,title, dbTableName

 */
public class ActivityCover extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Animation.AnimationListener, TabLayout.OnTabSelectedListener {

    int numTabs, currTab, currItem;
    List<TabItem> tabItemList;
    List<NavigationMenuItem> extraMenuItemList;
    boolean onAnimation;
    int final_cover_id=-1;

    //MyAdapter mAdapter;
    ViewPager mPager;

    ImageView coverImage;
    TextView coverTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        buildArguments();  // 스타일 및 tabItemlist 및 extraMenuItemList 를 만든다
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cover);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //toolbar 및 navigationView 를 customization한다
        buildHeaders(toolbar, navigationView);

        mPager = (ViewPager)findViewById(R.id.viewPager);
        setupViewPager(mPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(mPager);
        tabLayout.setOnTabSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        //List<Integer> extra_id_list = (List<Integer>) CollectionUtils.collect(extraMenuItemList, new BeanToPropertyValueTransformer(""));
        //if(extraMenuItemList.contains())
        menuPageOpen(id);  //여기서 chain으로 연결되어져 extraMenuPageOpen에서 새로이 추가된 id로 열어준다
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void buildArguments(){
        /*
            intent나 sharedPreference로 입력된 내용을 기반으로 기본 자료들을 준비한다
            style(font) & theme,
            tabList : tabName, subTitle, imgUrl, db_table_name
            navigationViewExtraMenuItem : name, menu_id, action_string, intent
         */

        //기본으로 나오는 것들은 여기에 두고 ..그 다음에 추가 내용들을 sharedPreference로부터 추출한다 intent를 쓰지 않는 것이 좋겠다
        tabItemList.add(new TabItem("Seoul","k-Vib",String.valueOf(R.drawable.sky),"content"));  //tableName
        tabItemList.add(new TabItem("London","is calling","http://www.lufthansa.com/flights/destination-images/LON.jpg","content")); //tableName

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(this);
        String jsonString=sp.getString(Config.PREF_COVERLIST,"default");  //key, default
        if(jsonString.contentEquals("default")) {
            Log.i(Config.AppName,"preference coverlist empty"); return;
        }
        Gson gson = new Gson();
        TabItem[] tempArray = gson.fromJson(jsonString,TabItem[].class);
        for(TabItem tItem : tempArray) {
            tabItemList.add(tItem);
        }
        //tabItemList.add(new TabItem());  //
        numTabs=tabItemList.size();

        String jsonString2= sp.getString(Config.PREF_NAVIGATIONMENU_EXTRALIST,"default");
        if(jsonString.contentEquals("default")) {
            Log.i(Config.AppName,"preference extraMenulist empty"); return;
        }
        Gson gson2 = new Gson();
        NavigationMenuItem[] tempArray2 = gson2.fromJson(jsonString,NavigationMenuItem[].class);
        for(NavigationMenuItem mItem : tempArray2) {
            extraMenuItemList.add(mItem);
        }
        //extraMenuItemList.add(new NavigationMenuItem(String name, int id, String action));
        Log.i(Config.AppName,"coverSize["+tabItemList.size()+"],AddOns["+extraMenuItemList.size()+"]");
    }

    private void buildHeaders(Toolbar toolBar, NavigationView navigationView){
        //navigationView 헤더와 메뉴는 각각,@layout/nav_header_cover, @menu/activity_cover_drawer
        SharedPreferences sp=PreferenceManager.getDefaultSharedPreferences(this);
        //sp.getString(Config.PREF)
        ImageView navHeaderImageView= (ImageView) navigationView.findViewById(R.id.nav_header_image);
        navHeaderImageView.setImageURI(Uri.parse(""));
        TextView navHeaderTextView = (TextView) navigationView.findViewById(R.id.nav_header_userName);
        navHeaderTextView.setText(sp.getString(Config.PREF_USERNAME,"android"));
    }

    private void setupViewPager(ViewPager viewPager){
        MyAdapter adapter=new MyAdapter(getSupportFragmentManager());
        adapter.addFragment(buildFragment(-1,-1),new TabItem("name1","subTitle1","url","dbTableName"));
        adapter.addFragment(buildFragment(-1,-1),new TabItem("name2","subTitle2","url","dbTableName"));
        if(tabItemList!=null && tabItemList.size()>0){
            for(TabItem tb:tabItemList){
                adapter.addFragment(buildFragment(-1,-1),tb);
            }
        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                changeCoverTab(position);
                currItem=-1;
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        viewPager.setAdapter(adapter);
    }

    private Fragment buildFragment(int option, int value){
        Fragment fragment = null;
        if(option ==-1){   //test sample용 더미 fragment , using data Cheese.sCheeseStrings
            fragment=ArrayListFragment.newInstance(1);
        }
        return fragment;
    }

    private void changeCoverTab(int coverId){
        //페이지가 바뀌면, 이미지와 중앙 타이틀도 바꾸어준다.말려있는 상태면 바뀌어주어도 보이는 변화는 없을 것임
        TabItem tabItem= tabItemList.get(coverId);
        if(onAnimation){
            final_cover_id=coverId;
        }else{
            onAnimation=true;
            Animation animation= AnimationUtils.loadAnimation(this,R.anim.fade_in);
            coverTitle.startAnimation(animation);
            coverImage.startAnimation(animation);
            coverTitle.setVisibility(View.GONE);
            coverTitle.setText(tabItem.tabName);
            Glide.with(this).load(tabItem.imgUrl).into(coverImage);   //.load(R.raw.splash)
            //coverImage.setImageURI(Uri.parse(tabItem.imgUrl));  //imageview.setImageResource(R.drawable.image);
            Animation animation_fade_out=AnimationUtils.loadAnimation(this,R.anim.fade_out);
            animation_fade_out.setAnimationListener(this); //다 끝나면 call back -onAnimationEnd
            coverImage.startAnimation(animation_fade_out);
            coverTitle.setVisibility(View.VISIBLE);
            final_cover_id=-1;
            onAnimation=false;
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {}
    @Override
    public void onAnimationRepeat(Animation animation) {}
    @Override
    public void onAnimationEnd(Animation animation) {
        if(final_cover_id!=-1){
            changeCoverTab(final_cover_id);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        Log.i(Config.AppName, "tabSelected["+position+"]");
        if(currTab == position){ return;}
        changeCoverTab(position);
        mPager.setCurrentItem(position,true);
    }
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}
    @Override
    public void onTabReselected(TabLayout.Tab tab) {}


    static class MyAdapter extends FragmentPagerAdapter {

        public final List<Fragment> mFragments = new ArrayList<>();  //final prevents you from reassigning Object after you've assigned it once - you can still add/remove elements as you would normally.
        public final List<TabItem> tabItems = new ArrayList<>();

        @Override
        public Fragment getItem(int position) {
            //bundle.putString(), fragment.setArguments(bundle);
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            if(mFragments.size()!=tabItems.size()){ Log.w(Config.AppName,"error on CoverAdapter");}
            return mFragments.size();  //numTabs
        }

        @Override
        public CharSequence getPageTitle(int position){
            return tabItems.get(position).tabName;
        }

        public void addFragment(Fragment fragment, TabItem tabItem){
            mFragments.add(fragment);
            tabItems.add(tabItem);
        }

        public MyAdapter(FragmentManager fm) { super(fm); }

    }

    private void menuPageOpen(int request){
        Intent intent=null;

        switch(request){
            //case: break;  //intent를 만들던가 다른 페이지로 이동하면 된다 changeCover(int); & move

            default:
                Log.i(Config.AppName,"no matching menu id from default List thus calling extra menu matcher");
                intent=extraPageOpen(request); //새로 추가된 menu는 여기서 다시 함수를 호출하여 해결한다
                break;
        }
        //더넣고 싶으면 intent.putExtra(requestCode, reqCode);
        if(intent!=null && intent.resolveActivity(getPackageManager())!=null){
            startActivity(intent);
        }
    }

    private Intent extraPageOpen(int request){
        Intent intent = null;
        //case R.id.int, intent.putExtra(frgamentId, int value)
        return intent;
    }

    class TabItem {
        public String tabName;
        public String subTitle;
        public String imgUrl;
        public String tableName;

        public TabItem(String tabName, String subTitle, String imgUrl, String tableName){
            this.tabName=tabName; this.subTitle=subTitle; this.imgUrl=imgUrl; this.tableName=tableName;
        }
    }

    class NavigationMenuItem {
        public String name;
        public int icon;  //icon  id
        public int id;  //menu id
        public String action;
        public Intent intent;

        public NavigationMenuItem(String name, int icon, int id, String action, Intent intent ){
            this.name=name; this.icon=icon; this.id=id; this.action=action; this.intent=intent;
        }

        public NavigationMenuItem(String name, int id, String action){
            this.name=name; this.id=id; this.action=action;
        }
    }



}
