package com.kewtea.urban.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kewtea.urban.R;

import java.util.List;

/**
 * Created by jungh on 7/14/16.

    compatible with viewPager for suport v4 library by using app4.fragment
    can set list, grid or staggered grid layout
    for different card styles in each cover , please configure via CoerRecycleAdapter


    usage : CoverRecycleFragment fragment = new CoverRecycleFragment();
            Bundle bundle ..bundle.putInt(Key,Value).....fragement.setArgument(bundle);
            bundle => LayoutManager, LayoutStyle, setAdapter
            FragmentManager.Tranaction.add/replace(layoutView,fragment).commit():

    assign Templates
        T0 : list default
        T1 :
        T2 :
        T3 :
        T4 :
        T5 :
        T6 :


    assign Data from ContentProvider or from example

 swipeayout : https://guides.codepath.com/android/Implementing-Pull-to-Refresh-Guide -add to Adapter notifyDataSetChanged();




 */
public class CoverRecycleFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeLayout;
    private RecyclerView recyclerView;
    private CoverRecycleAdapter adapter;
    private int layoutstyle;
    private int layoutId;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //View view = inflater.inflate(R.layout.fragment_cover_recycle, container,false);
        View view = inflater.inflate(layoutId, container,false);

        //added swipeRefresh
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeLayout.setOnRefreshListener(this);
        // Configure the refreshing colors
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RecyclerView rv=(RecyclerView) view.findViewById(R.id.recyclerView);
        //rv.setHasFixedSize(true); //optional performance enhancement
        RecyclerView.LayoutManager llm;

        /*
        switch(layoutstyle){
            case LinearLayout:
                llm=new LinearLayoutManager(getActivity());
                break;
            case GridLayout:
                llm=new GridLayoutManager(getActivity(),2);
                break;
            case StaggeredGridLayout:
                llm=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
                break;
            default:
                llm=new LinearLayoutManager(getActivity());
                break;
        }
        */
        llm=new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        //rv.setAdapter
        return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void init(){
        //1. check bundle  & set grid/list layout template
        Bundle bundle=getArguments();
        if(bundle!=null){
            //layoutstyle=bundle.getInt(LayoutStyle,1);
            //layoutId=bundle.getInt(LayoutRid,R.layout.fragment_cover_recycle);
        }

        //2. load Data from ContentProvider
        List<CoverRecycleAdapter.CoverItem> coverList=loadData("");

        //3. build adapter
        adapter= new CoverRecycleAdapter(getActivity(), coverList);


    }

    private List<CoverRecycleAdapter.CoverItem> loadData(String tableName){

        return null;
    }

    @Override
    public void onRefresh() {
        //do some action from swipeRefresh

        //Update the adapter and notify data set changed
        //Stop refresh animation
        //swipeLayout.setRefreshing(false);  //아예동작을 안하게 하려면 setEnabled(false)

    }
}
