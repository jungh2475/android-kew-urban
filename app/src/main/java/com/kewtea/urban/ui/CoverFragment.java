package com.kewtea.urban.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kewtea.urban.R;

import java.util.List;

/**
 * Created by junghlee on 7/17/16.


 usage : CoverRecycleFragment fragment = new CoverRecycleFragment();
 Bundle bundle ..bundle.putInt(Key,Value).....fragement.setArgument(bundle);
 bundle => LayoutManager, LayoutStyle, setAdapter
 FragmentManager.Tranaction.add/replace(layoutView,fragment).commit():

 swipeayout : https://guides.codepath.com/android/Implementing-Pull-to-Refresh-Guide -add to Adapter notifyDataSetChanged();


 */
public class CoverFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeLayout;
    private RecyclerView recyclerView;
    private CoverRecycleAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cover, container,false);

        swipeLayout =(SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeLayout.setOnRefreshListener(this);
        // Configure the refreshing colors
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        recyclerView=(RecyclerView) view.findViewById(R.id.recycler_view);
        //rv.setHasFixedSize(true); //optional performance enhancement
        RecyclerView.LayoutManager llm;
        llm=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter); //rv.setAdapter.........
        return view;
    }

    private void init(){
        //1. check bundle  & set grid/list layout template
        Bundle bundle=getArguments();
        if(bundle!=null){
            //layoutstyle=bundle.getInt(LayoutStyle,1);
            //layoutId=bundle.getInt(LayoutRid,R.layout.fragment_cover_recycle);
        }

        //2. load Data from ContentProvider
        List<CoverRecycleAdapter.CoverItem> coverList=loadData("");

        //3. build adapter
        adapter= new CoverRecycleAdapter(getActivity(), coverList);


    }

    private List<CoverRecycleAdapter.CoverItem> loadData(String tableName){

        return null;
    }


    @Override
    public void onRefresh() {
        //do some action from swipeRefresh

        //Update the adapter and notify data set changed
        //Stop refresh animation
        //swipeLayout.setRefreshing(false);  //아예동작을 안하게 하려면 setEnabled(false)

    }

}
