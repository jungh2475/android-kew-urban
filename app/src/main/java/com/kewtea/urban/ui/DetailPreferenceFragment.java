package com.kewtea.urban.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kewtea.urban.R;

/**
 * 할일들
    - toolbar를 추가한다 : detail_activity에서 collapsing toolbar 없는 버젼으로 -> fragmentmanager,inflate.commit (v4->v13으로 바꿀 필요가 있는가?)
    - sharedPreference로 동적으로 메뉴 추가 : add_ons : journal, spirithouse, (spot), kewrator, kewchloe
    - 설정 값을 화면에 보이게 한다 - replace subtitle with values : userid, color, language, version of sw
    - 추가로 열어야 할 화면들은 ? : EULA, OpenSourceLicense, About Developer

 registerOnSharedPreferenceChangeListener

 * Created by junghlee on 7/10/16.
 *
 * Android support v4 does not support PreferenceFragment - 2015
 * https://developer.android.com/reference/android/preference/PreferenceFragment.html
 *
 * fragment : onAttach => onCreate => onCreateView => onViewCreated =>onActivityCreated =>onStart =>onResume
 */
public class DetailPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {


    SharedPreferences settings;
    PreferenceScreen screen;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);

    }

    /* 앞의 addPreferencesFromResource(R.xml.preference)으로 이미 대치되어서 onCreateView 필요 없음. 대신에  onViewCrated 실행함
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_preference, container, false);
        return view;
    }
    */


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        settings=PreferenceManager.getDefaultSharedPreferences(getActivity());
        //get sharedPreference
        loadData();
        screen = this.getPreferenceScreen();
        PreferenceCategory category = new PreferenceCategory(screen.getContext());
        category.setTitle("extra added later");
        screen.addPreference(category);
        CheckBoxPreference checkBoxPref = new CheckBoxPreference(screen.getContext());
        checkBoxPref.setTitle("hello test");
        category.addPreference(checkBoxPref);



    }



    private void loadData(){
        //TODO: part1 fetch from content provider
        //SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if(settings.contains("preference_language")){
            ListPreference listPref=(ListPreference) findPreference("preference_language");
            listPref.setSummary(settings.getString("preference_language","empty"));
        }

        if(settings.contains("preference_username")){
            Log.i("kewUrban:","username["+settings.getString("preference_username","empty"));
            EditTextPreference editText = (EditTextPreference) findPreference("preference_username");
            editText.setSummary(settings.getString("preference_username","empty"));
        }
        //TODO: part2 여기에 default 값이 있으면 summary에 보여 주어야 할것들을 다 적어 둔다
        //settings.registerOnSharedPreferenceChangeListener();
        //Preference에 defaultValue 저장 값을 읽어 올수가 없음
        Log.i("kewUrban:","swid["+settings.getString("preference_swId","null")+"]"+findPreference("preference_swId").getSummary());
        settings.edit().putString("preference_swId", getResources().getString(R.string.my_default_swid)).commit();
        Log.i("kewUrban:","swid["+settings.getString("preference_swId","null")+"]"+findPreference("preference_swId") .getSummary());

        if(settings.contains("preference_swId")){
            findPreference("preference_swId").setSummary(getResources().getString(R.string.my_default_swid));
        }

    }

    @Override
    public void onResume() {
        settings.registerOnSharedPreferenceChangeListener(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        settings.unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i("kewUrban:","value has been changed");
        Preference targetPreference = findPreference(key);
        if(targetPreference instanceof SwitchPreference){return;}
        targetPreference.setSummary(settings.getString(key,"empty"));
        /*switch (key){
            case "":

                break;
            case "aa":
                break;
            default:
                break;
        }
        */
    }
}
