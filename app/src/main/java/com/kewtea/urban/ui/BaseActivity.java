package com.kewtea.urban.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.kewtea.urban.Config;
import com.kewtea.urban.service.MainService;

/**
 *  Created by junghlee on 8/3/16.
    MainService하고  callback을 할수 있는 Listener를 장착하였을뿐 그외는 별로 차이가 없
 음
 */
abstract class BaseActivity extends AppCompatActivity implements MainService.ProgressListener {

    boolean mBound=false;
    MainService mService;


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MainService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MainService.LocalBinder binder = (MainService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onResume() {
        if(mBound){mService.registerListener(this);}
        super.onResume();
    }

    @Override
    protected void onPause() {
        if(mBound){mService.removeListener(this);}
        super.onPause();
    }


}
