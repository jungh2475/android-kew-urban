package com.kewtea.urban.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.kewtea.urban.Config;
import com.kewtea.urban.MainActivity;
import com.kewtea.urban.R;
import com.kewtea.urban.service.SystemReceiver;

/**
 * Created by junghlee on 7/21/16.
 */
public class StartActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //setContentView(R.layout.splash_screen);  //이부분을 삭제해도 됨  splash Style이 적용되어져 있기때문에..
        //startAnimations();
        /*
            If you did have a layout file for your splash activity, that layout file would be visible to the user only after your app has been fully initialized, which is too late.
            You want the splash to be displayed only in that small amount of time before the app is initialized.
         */

        Intent intent=whichActivityStart();
        if(intent!=null){startDelayActivity(intent,1500);}
    }

    private Intent whichActivityStart(){
        /*
           어떤 activity를 실행 할지 결정하는 logic (cover-withwithoutServerUpdate, intro, login/signup)
            account 없으면 =>intro, 로그인해본적 있는 경우, 최근 접속이 10분이내=>cover, 마지막 로그인 시간이 5시간 이상=>cover with spinner(server downloading kicks)
            account 있으나 마지막 로그인한 시간이 없는 경우 => signUp
         */

        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(this);
        if(pref == null ) {
            Log.w("kewUrban:","failed to load sharedPreference");
            return null;
        }

        Long lastLogInTime = pref.getLong(Config.PREF_LAST_LOGIN_TIME, -1L);
        String accountName = pref.getString(Config.PREF_USERNAME,"");

        Intent intent;

        //timeGap
        if(accountName.equals("")){
            Log.i(Config.AppName,"starting...intro");
            intent = new Intent(this,IntroActivity.class);// go to intro activity from page1
            //if(lastLogInTime==-1L){intent.putExtra()}
        }else{
            Log.i(Config.AppName,"starting...cover");
            Long timeGap = System.currentTimeMillis()-lastLogInTime;
            if(timeGap>5*60*60*1000){
                intent = new Intent(this, CoverActivity.class);// go to cover with data refresh
                intent.putExtra("","");
            }else {
                intent = new Intent(this, CoverActivity.class);// go to cover - simple
            }
        }

        intent = new Intent(this, MainActivity.class); //비상용 코드 임 나중에 없애야함 intent=null;
        return intent;

    }


    private void startDelayActivity(final Intent intent, int delaytime){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(intent);
                finish();
            }
        }, delaytime);
    }

    private void startAnimations() {
        //화면이 밝아 지면서 이미지가 올라 오는 모양
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.splash_alpha);
        anim.reset();
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.splash_layout);
        //LinearLayout l = (LinearLayout) findViewById(R.id.splash_layout);
        layout.clearAnimation();
        layout.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.splash_translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.imgLogo);
        iv.clearAnimation();
        iv.startAnimation(anim);
    }
}
