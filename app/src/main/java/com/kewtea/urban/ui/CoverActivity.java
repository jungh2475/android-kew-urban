package com.kewtea.urban.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.TableLayout;

import com.kewtea.urban.Config;
import com.kewtea.urban.R;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by jungh on 7/4/16.
    extends from MainActivity.java - navigationDraw, CollapsingToolbar, viewPager, recycleCardView (+ expandible or flip)
    tabLayout - fragmentstatepager, fragmentPager
    http://www.truiton.com/2015/06/android-tabs-example-fragments-viewpager/
    http://susemi99.kr/3399

 */
public class CoverActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    /*
        numTabs, currTab(page), currItem  <= load from sharedPreference
        MyViewAdapter mAdapter
        ViewPager mPager

     */
    int numTabs, currTab, currItem;
    List<TabItem> tabItemList;
    List<ExtraMenuItem> extraMenuItemList;

    //MyAdapter mAdapter;
    ViewPager mPager;
    SharedPreferences settings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        buildArguments();



        super.onCreate(savedInstanceState);

        //MainActivity에서 이것이 호출이 되어져야 합니다.
        //PreferenceManager.setDefaultValues(this, R.xml.preference, false);
        settings = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_cover);

        initToolbar();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        loadData();  //build TabList and Navigation View Extra Menu Items

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //extra menu 추가
        if(extraMenuItemList !=null && extraMenuItemList.size()>0){
        //if(settings.getBoolean(Config.EXTRA_ADD_ON,false)){
            final Menu menu=navigationView.getMenu();
            final SubMenu subMenu= menu.addSubMenu("Addons");
            for(ExtraMenuItem item : extraMenuItemList){
                final MenuItem menuItem = subMenu.add(subMenu.getItem().getGroupId(), item.id,0, item.name);
            }
            //final MenuItem mItem=subMenu.add(R.id.nav_group1,578,0,"hihi11");
            //menu.add(R.id.nav_menu_2,234,Menu.NONE,"hello2");
            //TODO: 색상을 입혀서 highlight해야한다
            // add click behaviour at onNavigationItemSelected
        }


        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        //cover에서는 중앙에 나오게 하여야한다. 아래의 코드는 아래에 붙게 만든다
        //collapsingToolbarLayout.setTitle("My Toolbar Title");
        //collapsingToolbarLayout.setContentScrimColor(Color.GREEN);
        //NestedScrollView scrollView = (NestedScrollView) findViewById (R.id.nest_scrollview); scrollView.setFillViewport (true);

        initViewPagerAndTabs();

        mPager = (ViewPager)findViewById(R.id.viewPager);
        setupViewPager(mPager); //여기에서 tabList도 같이 만들어 줌

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        //tabLayout.setTabMode(TabLayout.MODE_FIXED);
        //tabLayout.addTab(tabLayout.newTab().setText("Tab 1")); //그냥 일반 tab을 넣을 때는 이런식으로 ...
        /*
          styling guide: https://guides.codepath.com/android/google-play-style-tabs-using-tablayout
            (tabMode=fixed, tabGravity=fill),  (tabMode=scrollable, tabGravity=centre)
        */
        tabLayout.setupWithViewPager(mPager);
        //setCoverPageTab(0);

    }

    private void buildArguments(){
        //get intent and build arguement - styling/layout/default values
    }
    private void initToolbar(){
        //styling toolbar + collapsing

        //add Fab button

        //styling navigationDrawer : https://medium.com/android-news/navigation-drawer-styling-according-material-design-5306190da08f#.9ybpegfqq
    }
    private void initViewPagerAndTabs(){

    }

    private void changeCover(int coverId){
        //페이지가 바뀌면, 이미지와 중앙 타이틀도 바꾸어준다.

    }

    private void loadData(){

        //load from SharedPreference, build tabList, navigationview menu items
        //TODO: load from SharePreference
        //SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String jsonString = settings.getString(Config.EXTRA_ADD_ON, "");
        TabItem tabTemp = null; //parseJason
        //for (tabItemList.add(new TabItem(tabTemp.tabName, tabTemp.subTitle, tabTemp.imgUrl,tabTemp.tableName));)
        ExtraMenuItem menuTemp=null;
        //for(extraMenuItemList.add(new ExtraMenuItem(menuTemp.name, menuTemp.id, menuTemp.action));)

        tabItemList.add(new TabItem("Today's Pick","Selected For You","url","content.top"));
        tabItemList.add(new TabItem("Today's Pick","Selected For You","url","content.top"));
        numTabs=tabItemList.size();
        extraMenuItemList.add(new ExtraMenuItem("Kewtea Post", 56,"content.post"));
        extraMenuItemList.add(new ExtraMenuItem("Kewtea Kewrator", 57,"content.kewrator"));

    }

    class TabItem {
        public String tabName;
        public String subTitle;
        public String imgUrl;
        public String tableName;

        public TabItem(String name, String subTitle, String url, String tableName){
            this.tabName=name; this.subTitle=subTitle; this.imgUrl=url; this.tableName=tableName;
        }
    }

    class ExtraMenuItem {
        public String name;
        public int id;  //menu id
        public String action;
        public Intent intent;

        public ExtraMenuItem(String name, int id, String action){
            this.name=name; this.id=id;
            this.action=action;
        }

    }

    private void setupViewPager(ViewPager viewPager){
        MyAdapter adapter= new MyAdapter(getSupportFragmentManager());

        //여기에서 tabList에 따라서 fragment List를 만들면서 adapter에 추가한다
        if(tabItemList!=null && tabItemList.size()>0){
        //if(settings.getBoolean("preference_extra_addons",false)){
            for(int i=0; i<tabItemList.size(); i++) {
                adapter.addFragment(buildFragment(tabItemList.get(i).tableName), tabItemList.get(i).tabName);
            }
        }
        //수작업인 경우 : adapter.addFragment(ArrayListFragment.newInstance(0),"frag1");

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                currItem=-1;
                changeCover(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        viewPager.setAdapter(adapter);
    }

    private Fragment buildFragment(String option){
        Fragment fragment= null;
        Bundle bundle= new Bundle();
        switch(option){
            case "aaa":
                fragment = new CoverFragment();
                //bundle.putInt(CoverRecycleFragment.LayoutStyle, CoverRecycleFragment.LinearLayout);
                fragment.setArguments(bundle);
                break;
            case "":
                fragment = new CoverRecycleFragment();
                //fragBundle.putInt(CoverRecycleFragment.LayoutStyle, CoverRecycleFragment.LinearLayout);
                fragment.setArguments(bundle);
                break;
            default:
                fragment=ArrayListFragment.newInstance(5);
                break;
        }

        return fragment;
    }

    public static class MyAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();  //tabList에 보이는 이름들임

        Fragment fragment;
        public MyAdapter(FragmentManager fm) { super(fm); }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public int getCount() {
            return mFragments.size();  //numTabs
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }

        @Override
        public Fragment getItem(int position) {  //tab에 따른 화면fragment들을 보내줌
            //by city, recyclecardview(rCv)
            //num 4 is avPlaylist, num 5 is gMap
            //bundle.putString(), fragment.setArguments(bundle);
            //arrayList<Fragment>에서 맞는 그림을 가져다가 줄수도 있다
            //Fragment fragment = ArrayListFragment.newInstance(position);
            //return fragment;
            return mFragments.get(position);
        }
    }

    public void setCoverPageTab(int pos){
        mPager.setCurrentItem(pos, true);  //smooth scrolling
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks.
        int id = item.getItemId();
        Log.i("KewUrban:","id["+id);

        pageOpen(id);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void pageOpen(Intent intent){

    }

    private void pageOpen(int req){
        Intent intent = null;

        switch(req){
            case R.id.nav_cover:
                intent = new Intent(getApplicationContext(), CoverActivity.class);
                break;
            case R.id.nav_detail:
                intent = new Intent(getApplicationContext(), DetailActivity.class);
                break;
            case R.id.nav_preference:
                intent = new Intent(getApplicationContext(), Detail2Activity.class);
                intent.putExtra("fragmentId",R.id.nav_preference);
                break;
            case R.id.nav_intro:
                intent= new Intent(getApplicationContext(),IntroActivity.class);
                break;
            case R.id.nav_login:
                intent = new Intent(getApplicationContext(), Detail2Activity.class);
                intent.putExtra("fragmentId",R.id.nav_login);
            default:
                //TODO: resolve newly added item ids here
                Log.i("KewUrban:","no_match with id["+req);
                break;
        }
        //intent.putExtra(requestCode, reqCode);
        if(intent!=null && intent.resolveActivity(getPackageManager())!=null) {
            startActivity(intent);
        }
    }

}
