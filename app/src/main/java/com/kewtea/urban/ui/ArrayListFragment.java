package com.kewtea.urban.ui;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kewtea.urban.R;

/**
 * Created by jungh on 7/6/16.
 */
public class ArrayListFragment extends ListFragment {

    static ArrayListFragment newInstance(int num) {
        ArrayListFragment f = new ArrayListFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.array_list_fragment, container, false);
        //View tv = v.findViewById(R.id.text); ((TextView)tv).setText("Fragment #" + mNum);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setNestedScrollingEnabled(true);  //중요! collapsingToolbar때문에
        setListAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, Cheese.sCheeseStrings));  //Cheeses.sCheeseStrings
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("FragmentList", "Item clicked: " + id);
    }

}
