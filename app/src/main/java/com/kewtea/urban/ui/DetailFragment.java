package com.kewtea.urban.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.kewtea.urban.R;

/**
 * Created by jungh on 7/15/16.

    onAttach => onCreate** =>onCreateView** =>onActivityCreated =>onStart =>onResue
    onPause => onStop =>onDestroyView => onDestroy => onDetach
 */
public class DetailFragment extends Fragment implements ViewTreeObserver.OnScrollChangedListener {

    NestedScrollView mScrollView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);  //?
        return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViewAndListeners();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_detail, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bookmark:
                //some action here
                return true;
            case R.id.menu_share:
                //some action here
                return true;
        }
        return false;
    }

    private void initViewAndListeners(){
        // mScrollView.addCallbacks(this)
        mScrollView.computeScroll();
        //mScrollView.setOnScrollChangeListener //Support Library 23.1 introduced an OnScrollChangeListener to NestedScrollView.
        mScrollView.getViewTreeObserver().addOnScrollChangedListener(this);  //old method
    }

    @Override
    public void onScrollChanged() {  //v23이상 interface는 x,y값이 변수로 들어오고, 이전것은 직접 찾아내야한다
        // mScrollView.getScrollX();
        // mScrollView.getScrollY();
    }


    private void displayData(String data){
        //title
        //subTitle
        //imgUrl
        //bodyText
        //footer
    }


}
