package com.kewtea.urban.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kewtea.urban.R;

import java.util.List;

/**
 * Created by jungh on 7/14/16.
 *
    총 몇개의 CoverCard Type이 있는가? 그리고 보여줄 데이터 형식은 어떤 것들인가?
    data => label, title, subTitle, shortDescription, imgUrl

    type1 : card_LeftBottomAlign
    type2 : card_centerAlign
    type3 : image+TextBottom
    type4 : imageLeft + TextRight
    type5 : tile, textOnly design

    layout_viewholder_type.xml(+id: title, subTitle, label, shortDescription, imgUrl)


 */
public class CoverRecycleAdapter extends RecyclerView.Adapter<CoverRecycleAdapter.MyViewHolder> {


    public class CoverItem {

        public String title;
        public String subTitle;
        public String imgUrl;
        public String label;
        public int cardType;

        public CoverItem(String title, String imgUrl){
            this.title=title; this.imgUrl=imgUrl;
        }
        public CoverItem(String label, String title,String subTitle, String imgUrl, int type){
            this.title=title; this.imgUrl=imgUrl;
            this.label=label; this.subTitle=subTitle; this.cardType=type;
        }

    }

    public static final int CoverCard_Text=1;  //hGroup only
    public static final int CoverCard_ImgFull=2; //imgfull + centre Hgroup
    public static final int CoverCard_List=3;  //hGroupd left, photo right
    public static final int CoverCard_ImgTop=4;  //imgTop, hgroup bottom
    public static final int CoverCard_Slide=5;  //slide with imgFull

    private Context mContext;
    private List<CoverItem> coverList;

    @Override
    public int getItemViewType(int position) {
        return coverList.get(position).cardType;
        //return super.getItemViewType(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        MyViewHolder viewHolder=null;
        switch(viewType){
            case CoverCard_Text:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_txt, parent, false);//cover_card1,
                //LayoutInflater.from(viewGroup.getContext()).inflate(R.layout, parent,false)
                viewHolder = new Card1ViewHolder(view);
                break;
            case CoverCard_ImgFull:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_txt, parent, false);//cover_card2
                viewHolder = new Card1ViewHolder(view);
            case CoverCard_List:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_txt, parent, false);//cover_card3
                viewHolder = new Card1ViewHolder(view);
                break;
            case CoverCard_ImgTop:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_txt, parent, false);//cover_card4
                viewHolder = new Card1ViewHolder(view);
                break;
            case CoverCard_Slide:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_txt, parent, false);//cover_card1
                viewHolder = new Card1ViewHolder(view);
                break;
            default:
                Log.w("kewUrban:","no match in viewHolder");
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        switch(holder.getItemViewType()){
            case CoverCard_Text:
                Card1ViewHolder vholder = (Card1ViewHolder) holder;
                vholder.title.setText(coverList.get(position).title);
                vholder.subTitle.setText(coverList.get(position).subTitle);
                break;
            case CoverCard_ImgFull:
                break;
            case CoverCard_List:
                break;
            case CoverCard_ImgTop:
                break;
            case CoverCard_Slide:
                break;
            default:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return coverList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public MyViewHolder(View view){
            super(view);
        }
    }

    //Multiple viewHolders extends above

    public class Card1ViewHolder extends MyViewHolder{

        //hGroup only
        TextView title;
        TextView subTitle;
        public Card1ViewHolder(View view){
            super(view);
            this.title=(TextView) view.findViewById(R.id.cardview);//card_title
        }
    }


    public class Card2ViewHolder extends MyViewHolder{

        //imgfull + centre Hgroup

        public Card2ViewHolder(View view){
            super(view);
        }
    }

    public class Card3ViewHolder extends MyViewHolder{

        //hGroupd left, photo right

        public Card3ViewHolder(View view){
            super(view);
        }
    }

    public class Card4ViewHolder extends MyViewHolder{

        //imgTop, hgroup bottom

        public Card4ViewHolder(View view){
            super(view);
        }
    }

    public class Card5ViewHolder extends MyViewHolder{

        //slideshow

        public Card5ViewHolder(View view){
            super(view);
        }
    }



    public CoverRecycleAdapter(Context mContext, List<CoverItem> coverList){
        this.mContext=mContext; this.coverList=coverList;
    }


}
