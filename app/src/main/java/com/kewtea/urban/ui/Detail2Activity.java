package com.kewtea.urban.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.kewtea.urban.R;

/**
 * Created by jungh on 7/11/16.
 */
public class Detail2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail2_toolbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //getSupportActionBar().setHomeAsUpIndicator(); //manifest.xml =>android:parentActivityName="

        if(findViewById(R.id.fragment_container) == null){Log.w("kewUrban:","fragment Container missing"); return; }
        if(savedInstanceState !=null){Log.w("kewUrban:","savedInstance Existing what?"); return;}
        Intent intent= getIntent();
        if(intent.getExtras()==null){Log.w("kewUrban:","fragment Id missing"); return;}
        Bundle bundle = intent.getExtras();
        int fragmentId = bundle.getInt("fragmentId");


        if(fragmentId == R.id.nav_login ){
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, loadFragmentv4(fragmentId))
                    .commit();
        } else{
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, loadFragment(fragmentId))
                    .commit();
        }



    }

    private android.support.v4.app.Fragment loadFragmentv4(int fragmentId){
        android.support.v4.app.Fragment fragment=null;
        switch(fragmentId){
            case R.id.nav_login:
                break;
        }
        fragment= new LoginFragment();
        Bundle bundle=new Bundle();
        bundle.putInt("signType",1);
        fragment.setArguments(bundle);
        getSupportActionBar().setTitle("Login");
        return fragment;
    }

    private Fragment loadFragment(int fragmentId){

        Fragment fragment=null;

        switch(fragmentId){
            case R.id.nav_preference:
                fragment= new DetailPreferenceFragment();
                getSupportActionBar().setTitle("Settings");
                break;
            case R.id.nav_login:
                Log.w("kewUrban:","error FragmentType");
            default:
                Log.i("kew", "error");
                break;
        }

        return fragment;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
