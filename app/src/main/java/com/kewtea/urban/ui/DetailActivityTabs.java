package com.kewtea.urban.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kewtea.urban.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jungh on 7/20/16.

 detailPage이기는 하지만 하위에 또 tab들이 있고, viewPager가 있어서 한단계가 더 추가된 경우 임
 https://mzgreen.github.io/2015/06/23/How-to-hideshow-Toolbar-when-list-is-scrolling(part3)/
 fab animation: https://guides.codepath.com/android/floating-action-buttons
 3rd party fab lib: https://github.com/makovkastar/FloatingActionButton/blob/master/README.md
 */
public class DetailActivityTabs extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_AppBarOverlay);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tabs);

        buildArguments();
        initToolbar();
        initViewPagerAndTabs();

    }

    private void buildArguments(){}

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.app_name));
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        //fab.show()/hide(); //이것은 layout xml에서 app:behavior로 custom java class에 mapping 되어져 있음(ScrollAwareFABBehavior)

    }

    private void initViewPagerAndTabs(){
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        //pagerAdapter.addFragment(PartThreeFragment.createInstance(20), getString(R.string.tab_1));
        //pagerAdapter.addFragment(PartThreeFragment.createInstance(4), getString(R.string.tab_2));
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
        @Override
        public CharSequence getPageTitle(int position) {

            return fragmentTitleList.get(position);
        }
    }


}
