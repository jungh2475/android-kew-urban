package com.kewtea.urban.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

import java.util.List;

/**
 * Created by jungh on 7/13/16.

        http://blog.udinic.com/2013/07/24/write-your-own-android-sync-adapter/
        main properties: syncable and auto-sync (SyncAdapters with auto-sync=ON will be triggered every 24 hours)
        But, they can also run when there is a change to our data, notified by the content provider

        configure all from ContentResolver....cancelSync,getCurrentSync,setSyncAutomatically, addPeriodicSync, requestSync(refresh)
        ContentResolver.requestSync(ACCOUNT, AUTHORITY, null);  => kicks onPerformSync()
        MainService 안에 innerclass로 observer를 만들고, mResolver.registerContentObserver(mUri, true, observer);
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    Context mContext;
    private final  AccountManager accountManager;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        accountManager = AccountManager.get(context);
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        accountManager = AccountManager.get(context);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        //A+B를 해서 local에서는 B에만 있는 것을 저장하고, 서버에는 A에만 있는 것을 저장해 준다
        //prepare Token

        //accountManager.getAuthToken(account,"",)
        //String authToken = accountManager.blockingGetAuthToken(account, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS, true);  //which is doing the same as getAuthToken_() _but done synchronously

        //try catch
        int op_mode=1;
        switch(op_mode){
            case 1:  //downlink only check & store
                //download ->getLocal ->compare ->if Any, update LocalContentProvider
                break;
            case 2:   //uplink only check & store (upload 됫다고  clear 해줌
                // getLocal ->upload & clean local & clear uploadList
                break;
            case 3: //bidirectional 양방향 sync ...이것이 원래는 정석임
            default:
                break;
        }
    }

    public void downlinkSync(){

    }

    public void uplinkSync(){

    }

    public void putLocalDb(List<?> inList){
        //mContext.getContentResolver().
    }
    public void getLocalDb(){}
    public void getFromServer(){}
    public void putServer(){}


    public void compareDiff(List<String> as, List<String> bs, int option){

        List<String> as1=as;
        as1.removeAll(bs); //=as-bs
        List<String> bs1=bs;
        bs1.removeAll(as);  //=bs-as

        List<String> cs=as;
        cs.retainAll(bs);  //as && bs
        List<String> es=as;
        es.addAll(bs1);  //as+bs1

        switch(option){

        }

        //return cs or ds?
    }

    public void compareDiff(List<Long> aList, List<Long> bList){

    }

    public void notifyAuthFailure(){
        //service에서 Notification Bar를 보여주고 클릭하면 login을해서 정정하도록 한다

    }

    public void notifySynComplete(int resultCode){
        //정보들은 있으나, sync가 되어져 있어서 할 필요가 없는 경우(0), syncFailure(-1),
    }
}
