package com.kewtea.urban.sync;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.kewtea.urban.model.Content;

/**
 * Created by jungh on 7/13/16.

        http://blog.udinic.com/2013/04/24/write-your-own-android-authenticator/
        https://github.com/Udinic/AccountAuthenticator

        https://developer.android.com/reference/android/accounts/AccountManager.html
        AccountManager storage : Environment.getSystemSecureDirectory().getPath() + File.separator + DATABASE_NAME(=accounts.db);
        AccountManager => Accounts (type=kewtea, name), getAcconts, addAccount,getAuthToken
        am.addAccountExplicitly(account, password, extraDataBundle);  or void setUserData(Account account, String key, String value) => String myData = am.getUserData(account, "someKey");
        login 실패시 - invalidateAuthToken(String, String) =>immediately go back to the "Request an auth token"
        Constants for Bundle : KEY_INTENT, KEY_AUTHTOKEN, KEY_PASSWORD

        Account Manager with Google Oauth2 : https://www.learn2crack.com/2014/01/android-google-oauth2.html

        Store Credentials in Android AccountManager from google/twitter/facebook:
        https://github.com/KinveyApps/SignIn-Android -using library : http://devcenter.kinvey.com/android/downloads


 */
public class Authenticator extends AbstractAccountAuthenticator {

    private AccountManager accountManager;
    private Context mContext;

    public Authenticator(Context context) {
        super(context);
        mContext=context;
        accountManager=AccountManager.get(context);
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        throw new UnsupportedOperationException();
        //return null;
    }

    //
    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType,
                             String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
        //call login AccountAuthenticatorActivity  처음이면 로그인화면이 나오도록 함
        //accountManager.addAccount()

        final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
        /*
        intent.putExtra(AuthenticatorActivity.ARG_ACCOUNT_TYPE, accountType);
        intent.putExtra(AuthenticatorActivity.ARG_AUTH_TYPE, authTokenType);
        intent.putExtra(AuthenticatorActivity.ARG_IS_ADDING_NEW_ACCOUNT, true);
        */
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;

        //return null;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options)
            throws NetworkErrorException {

        return null;
    }

    //
    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account,
                               String authTokenType, Bundle options) throws NetworkErrorException {
        //accountManager.getAuthToken
        //TODO: getAuthToken을 해서 내용물이 없으면 AuthLoginActivity를 열어서 직접 로그인을 하게 한다. 몇번이상 실패하면 나오게 할까?
        //getAuthTokenByLogin();

        final AccountManager am = AccountManager.get(mContext);

        String authToken = am.peekAuthToken(account, authTokenType);

        // Lets give another try to authenticate the user
        if (TextUtils.isEmpty(authToken)) {
            final String password = am.getPassword(account);
            if (password != null) {
                /*
                authToken = sServerAuthenticate.userSignIn(account.name, password, authTokenType);
                */
            }
        }

        // If we get an authToken - we return it
        if (!TextUtils.isEmpty(authToken)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
            return result;
        }

        // If we get here, then we couldn't access the user's password - so we
        // need to re-prompt them for their credentials. We do that by creating
        // an intent to display our AuthenticatorActivity.
        final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        /*
        intent.putExtra(AuthenticatorActivity.ARG_ACCOUNT_TYPE, account.type);
        intent.putExtra(AuthenticatorActivity.ARG_AUTH_TYPE, authTokenType);
        */
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;

        //return null;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        throw new UnsupportedOperationException();


        //return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account,
                                    String authTokenType, Bundle options) throws NetworkErrorException {
        throw new UnsupportedOperationException();
        //return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response,
                              Account account, String[] features) throws NetworkErrorException {
        throw new UnsupportedOperationException();
        //return null;
    }

    private String getAuthTokenByLogin(){
        //token을 확보하면 token_acquired callback으로 부를ㄴ다
        return "";
    }



}
