package com.kewtea.urban.sync;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.kewtea.urban.R;

/**
 * Created by jungh on 7/13/16.

    AccountAuthenticatorActivity - Base class for the “sign-in/create account” activity to be called by the authenticator when the user needs to identify himself.
            setAccountAuthenticatorResult()-setAccountAuthenticatorResult(android.os.Bundle). This result will be sent as the result of the request when the activity finishes. If this is never set or if it is set to null then error ERROR_CODE_CANCELED will be called on the response.
    use layout template from http://developer.android.com/tools/projects/templates.html#login-activity

    https://developer.android.com/reference/android/accounts/AccountAuthenticatorActivity.html#setAccountAuthenticatorResult(android.os.Bundle)

 */
public class AuthenticatorActivity extends AccountAuthenticatorActivity {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }




    public void submit() {  //loginForm에서 submit한다
        /*
        final String userName = ((TextView) findViewById(R.id.accountName)).getText().toString();
        final String userPass = ((TextView) findViewById(R.id.accountPassword)).getText().toString();
        new AsyncTask<Void, Void, Intent>() {
            @Override
            protected Intent doInBackground(Void... params) {
                String authtoken = sServerAuthenticate.userSignIn(userName, userPass, mAuthTokenType);
                final Intent res = new Intent();
                res.putExtra(AccountManager.KEY_ACCOUNT_NAME, userName);
                res.putExtra(AccountManager.KEY_ACCOUNT_TYPE, ACCOUNT_TYPE);
                res.putExtra(AccountManager.KEY_AUTHTOKEN, authtoken);
                res.putExtra(PARAM_USER_PASS, userPass);
                return res;
            }
            @Override
            protected void onPostExecute(Intent intent) {
                finishLogin(intent);
            }
        }.execute();

        */

    }

    private void finishLogin(Intent intent) {
        String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        /*
        String accountPassword = intent.getStringExtra(PARAM_USER_PASS);
        final Account account = new Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));
        if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
            String authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            String authtokenType = mAuthTokenType;
            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager.addAccountExplicitly(account, accountPassword, null);
            mAccountManager.setAuthToken(account, authtokenType, authtoken);
        } else {
            mAccountManager.setPassword(account, accountPassword);
        }
        */
        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();  //Sends the result or a Constants.ERROR_CODE_CANCELED error if a result isn't present.
    }

    /*
    public void doAction(Bundle bundle){
        setAccountAuthenticatorResult(bundle);  //returned as the result of the AbstractAccountAuthenticator request
    }
    */
}
