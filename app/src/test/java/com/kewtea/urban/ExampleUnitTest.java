package com.kewtea.urban;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.

 By default, all tests run against the debug build type -can change this to another build type by using the testBuildType: build.gradle,
 src/main/
 src/androidTest/
 src/flavor1/
 src/androidTestFlavor1/
 android { ...  testBuildType "staging" }



 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

}