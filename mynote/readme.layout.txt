
Please read this document for drawing (kew-urban.layout.2016-1)
https://docs.google.com/document/d/1idAlGzO1w5LLuMy8OoqfVD052UMcguYxVN3VPBntlwg/edit?usp=sharing

[Layout-Cover-Sample1]
 - 맨밖에 DrawerLayout를 주고, nav_view를 drawer로 달아 준다
 - app_bar_main안에는 다시 CoordinatorLayout(appBar+toolBar)를 넣고, 그다음에 content_main이 오게한다

@layout.activity_main (support.v4.widget.DrawerLayout,@+id/drawer_layout)
    <include> @layout/app_bar_main (match_parent)

    @+id/nav_view : (header+menu)@layout/nav_header_main + @menu/activity_main_drawer

[Layout-Cover-Kew]
 - DrawerLayout(+NavigationView) => CoorinatorLayout>>AppBarLayout(ToolBar:Home_Hamburger+ImageView)+ViewPager
 -

[Layout-Detail]
..
[IconSource at]
https://design.google.com/icons  =>24dp default,  install at res/drawable/by_displayDefinitions
copy the standard Android icons from the SDK(${ANDROID_SDK_INSTALL_DIRECTORY}\platforms\android-${PLATFORM_VERSION}\data\res)
http://developer.android.com/design/downloads/index.html
android:icon="@android:drawable/ic_menu_save"  ...or myMenuItem.setIcon(android.R.drawable.ic_menu_save);

